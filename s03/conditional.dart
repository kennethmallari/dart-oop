void main() {
  print(ShowserviceItems('robotics'));
  print(determineTyphoonIntensity(50));
  print(selectSector(1));
  print(isUnderAge(12));
}
List<String> ShowserviceItems(String category) {
  if (category == 'apps') {
    return ['native', 'android', 'ios', 'web'];
  } else if (category == 'cloud') {
    return ['azure', 'microservices'];
  } else if (category == 'robotics') {
    return ['sensors', 'fleet-tracking', 'realtime-communication'];
  } else {
    return [];
  }
}

String determineTyphoonIntensity(int windSpeed) {
  if (windSpeed < 30) {
    return 'NOt a typhoon yet';
  } else if (windSpeed <= 61) {
    return 'Tropical Depression detected';
  } else if (windSpeed >= 62 && windSpeed <= 88) {
    return 'Tropical Storm detected';
  } else if (windSpeed >= 89 && windSpeed <= 117) {
    return 'Severe Tropical Storm detected';
  } else {
    return 'Typhoon Detected';
  }
}

String selectSector(int sectorId) {
  String name;
  switch (sectorId) {
    case 1:
      name = 'craft';
      break;
    case 2:
      name = 'Assembly';
      break;
    case 3:
      name = 'Building operations';
      break;
    default:
      name = sectorId.toString() + 'is out of bounds.';
  }

  return name;
}

bool isUnderAge(age) {
  return (age < 18) ? true : false;
  // return (age < 18) ; //pede most simplified
}