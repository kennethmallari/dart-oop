void main(){
  Person person = new Person(firstName: 'John', lastName: 'Smith');
  Employee employee = new Employee(firstName: 'Westwood', lastName: 'Jonas', employeeId: 'acmw-001');

  print(person.getFullName());
  print(employee.getFullName());

}

class Person{
    //Instance Variables:
    String firstName;
    String lastName;

    //Constructor:

    Person({required this.firstName, required this.lastName});

    String getFullName(){
      return '${this.firstName} ${this.lastName}';
    }

}

class Employee extends Person {

  // In spirit, the firstName and lastName inherited  by employee from Person
  // The Employee also inherits the getFullName from Person
  //Inheriitance then allowsus to write less code

//additional property of a sub class:
  String employeeId;

  Employee({
    required String firstName, 
    required String  lastName,
    required this.employeeId
  }) : super(
    firstName: firstName, 
    lastName: lastName
  );

}