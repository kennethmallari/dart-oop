//mixin = allows functionality without inheritance

void main() {
  Equipment equipment = new Equipment();
  equipment.name = 'Equipment-001';
  print(equipment.name);
  
  Loader loader = new Loader();
  loader.name = 'Loader-001';
  //print(loader.name);
  //print(loader.getCategory());
  loader.moveForward();
  loader.moveBackward();

  Car car = new Car();
  car.name = 'Crane-001';
  print(car.name);
  print(car.getCategory());
  car.moveForward();
  car.moveBackward();

}
class Equipment {
  String? name;
}
class Loader extends Equipment with Movement {
  String getCategory() {
    return '${this.name} is a loader.';
  }
}
class Car with Movement {
    String ? name;
  String getCategory() {
    return '${this.name} is a Car.';
  }
}

mixin Movement {
    void moveForward(){
        print('The vehicle is moving forward.');
    }

    void moveBackward(){
        print('The vehicle is moving backward.');
    }
}

