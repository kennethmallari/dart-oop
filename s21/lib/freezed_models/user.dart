import 'package:freezed_annotation/freezed_annotation.dart';

//You have a dart file will have the rest of its content in the user.freezed.dart
//parang you have different chapters in different files
// The rest of the contents for the user.dart can be found in user.freezed.dart using the 'part' keyword.
part 'user.freezed.dart';
part 'user.g.dart';

//The @freezed annotation  tells the build runner to create a freezed class named_$user in user.freezed.dart.
//makikita nya yung class na hdapat nyang basahin to generate code automatically. Without this annotation, walang code na magegenerate:
@freezed
class User with _$User {
    const factory User ({
        required int id,
        required String email,
        String? accessToken
    }) = _User;

    factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}