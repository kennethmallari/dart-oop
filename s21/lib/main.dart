import './models/user.dart';

void main() {
    //hashcode are dependent on the value. Hashcode is an integer
    //hashcode for primitives works differently.
    /*
    String x = '123';
    String y = '123';

    print(x == y);
    print(x.hashCode);
    print(y.hashCode);


     */
    
    User userA =  User(
        id: 1,
        email: 'john@gmail.com'
    );
    User userB =  User(
        id: 1,
        email: 'john@gmail.com'
    );

    // print(userA == userB);
    // print(userA.hashCode);
    // print(userB.hashCode);

    //To check for object equality, we need to do it like the code below. 

    // bool isIdSame = userA.id == userB.id;
    // bool isEmailSame = userA.email == userB.email;
    // bool areObjectsSame = isIdSame && isEmailSame;

    // print(areObjectsSame);

    // print(userA.email);
    // userA.email = 'john@hotmail.com';
    // print(userA.email);

}

//Dart programming dies not check the value of the object but also its unique identifier. (through hascodes);
//Freezed package makes it easier for us to handle objects in our code. 
//Kapag nag loko yung mga dependencies mo, try to delete .dart_tool to have a fresh install 

//sa node, delete node_modules the npm install ulit