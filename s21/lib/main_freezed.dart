import './freezed_models/user.dart';

void main() {
    
    
    User userA =  User(
        id: 1,
        email: 'john@gmail.com'
    );
    User userB =  User(
        id: 1,
        email: 'john@gmail.com'
    );

    //freezed makes comparison of objects with same properties and values to true
    print(userA == userB);
    print(userA.hashCode);
    print(userB.hashCode);

    //demonstartion of object immutability:
        //Immutability means changes are not allowed
        //mutable or immutable
        //Immutability ensures that an object will not be changed accidentally.
    //Instead of directly changing the object's property, the object itself will be changed.

    // print(userA.email);
    // userA.email = 'john@hotmail.com';
    // print(userA.email);

    //Instead of directly changing the object's property, the object itself will be changed. or re assigned with new values.
    //We achieve this by using the object.copyWith()
    //This comes with the freezed package: 
    // userA = userA.copyWith(email: 'john@hotmail.com');
    // print(userA.email);

    var response = {'id' : 3, 'email': 'doe@gmail.com'};

    //User userC = User(id: response['id'] as int, email: response['email'] as String);
    User userC = User.fromJson(response);
    print(userC);
    print(userC.toJson());

    

}

