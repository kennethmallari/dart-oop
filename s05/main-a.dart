void main(){
  Building building = new Building(name:'Caswyn', floors: 8, address: '134 Timog Ave, Brgy. Sacred Heart, Quexon City, Metro Manila');
  

  print(building);
  print(building.name);
  print(building.floors);
  print(building.address);

}

class Building{
  //instance variables:

  /** Before addressing the nullable errors **/

  //  String ?name;
  //  int ?floors;
  //  String ?address;
  
  /** After addressing the nullable errors**/

  //  String name;
  //  int floors;
  //  String address;
   

  //  //the name of the of the constructor is same as the name of the class:

  // Building(this.name, this.floors, this.address){
  //   this.name = name;
  //   this.floors = floors;
  //   this.address = address;
  //   print('A Building object has been created');
  // }


   String ? name;
   int floors;
   String address;
   

   //the name of the of the constructor is same as the name of the class:

  Building({ this.name, required this.floors, required this.address}){
   
    print('A Building object has been created');
  }
}


//The class declaration looks like this:

/*
class ClassName{
  <field>
  <constructor>
  <methods>
  <getters or setters>
}
 */