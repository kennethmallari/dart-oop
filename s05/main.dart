import './building.dart';
main(){
  Building building = new Building('Caswyn', floors: 8, address: '134 Timog Ave, Brgy. Sacred Heart, Quexon City, Metro Manila');
  

  // print(building);
  //print(building._name);

  //getter:
   print(building.Name);

   //setter:
     building.Name = 'GEMPC';
      print(building.Name);

  // print(building.floors);
  // print(building.address);

  print(building.getProperties());
}

// class Building{
//   //Instance variables:
//   //To hide the data (encapsulate) you should add '_' at the beginning of the variable name


//   String? _name;
//   int floors;
//   String address;

//   //Constructor:
//   Building(this._name,{required this.floors, required this.address}){
   
//     print('A Building object has been created');
//   }

//   //The get and set allows indirect access to class fields

//   String? get Name{
//     return this._name;
//   }

//   void set Name(String ? name){

//   }


//   //Methods
//   //pwedeng walang datatype yung return value ng methods
//   Map<String, dynamic> getProperties(){
//     return{
//         'name' : this._name,
//         'floors': this.floors,
//         'address' : this.address
//     };
//   }
// }