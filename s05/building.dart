//Required = when logically , you always require that variable have a value.
//A person will always have a first name and last name.
//Then use 'required' for both firstName and last name fields

//Use 'late' when logically, a variable can ave a null value.
//A person may not have a spouse.
// use 'late' if you are sure that your program, the value will be given to that variable.
//Else, use the null enabler instead(?)

void main(){

}

class Building{
  //Instance variables:
  //To hide the data (encapsulate) you should add '_' at the beginning of the variable name


  String? _name;
  int floors;
  String address;

  //Constructor:
  Building(this._name,{required this.floors, required this.address}){
   
    print('A Building object has been created');
  }

  //The get and set allows indirect access to class fields

  String? get Name{
    print('The building name has been retrieved. ');
    return this._name;
  }

  void set Name(String ? name){
    this._name = name;
    print('The building\'s name has been changed.');

  }


  //Methods
  //pwedeng walang datatype yung return value ng methods
  Map<String, dynamic> getProperties(){
    return{
        'name' : this._name,
        'floors': this.floors,
        'address' : this.address
    };
  }
}