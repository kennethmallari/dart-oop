//This will give access to material library
import 'package:flutter/material.dart';
//import the answer button custom widget:
import './body_questions.dart';
import'./body_answers.dart';

void main() {
  //App is the root widget
  runApp(new App());
}

//stateless widget is used when nothing will be changed
//Main widget:
class App extends StatefulWidget {
  @override 
  AppState createState() => AppState();
}

//nandito lahat ng code responsible for showing the UI. Widget for showing the question (body)
class AppState extends State<App>{
   int questionIdx = 0;
   bool showAnswers = false;
   final questions = [
       {
           'question' : 'What is the nature of your business needs?',
           'options': ['Time Tracking','Asset Management','Issue Tracking']
       },
       {
           'question' : 'What is the expected size of the user base?',
           'options': ['Less than 1,000','Less than 10,000','More than 1,000']
       },
       {
           'question':'In which region would the majority of the user base be?',
           'options': ['Asia','Europe','Americas','Africa','Middle East']
       },
       {
           'question' : 'What is the expected project duration?',
           'options': ['Less than 3 months','3-6 months','6-9 months','9-12 months','More tha 12 months']
       }
   ];

    var answers = [];

  //questions[0] => answers[0]
  void nextQuestion(String ? answer){
    answers.add({
        'question':questions[questionIdx]['question'],
        'answer':(answer == null)? '' : answer
    });

    if(questionIdx < questions.length -1){
        setState(() =>questionIdx++);
    }else{
        setState(()=> showAnswers = true);
    }
    
    print(answers);
  }
  //when the red squiggly line appears, hover the squiggly line , quick fix, then create....
  @override
  Widget build(BuildContext context) { 
        var bodyQuestions = BodyQuestions(questions: questions, questionIdx: questionIdx, nextQuestion: nextQuestion); 
        var bodyAnswers = BodyAnswers(answers: answers); 

        //responsible for building basic design and layout structure
        //The scaffold can be given UI elements such as an app bar
        Scaffold homepage  =Scaffold(
            appBar: AppBar(title:Text('Homepage')),
            body: (showAnswers)? bodyAnswers : bodyQuestions
        );
        return MaterialApp(
            //home is the index file
            home: homepage
        ); 
    }
}

/*
 //Two(2) types of widgets 
    //invisible
    //visible
    //The invisile widget are container and scaffold
    //The visible widgets are AppBar and Text
    //in flutter,width = double.infinity is the equivalent of width =100% in css
    //To put colors on a container, the decoration: BoxDecoration(Colors.green) can be used
    //in a Colum widget, the main axis alignment is vertical( or top to bottom)

    //To change the horizontoal alignment of widgets in a column, we must use the crossAxixAligment
    //for example, if you want to place column in widgets to the horizontal left, we use CrossAxisAlignment.start
    //In the context of the column, we can consider the column as vertical, and its cross axis alignment


    //to specify margin or spacing you may use EdgeInsets.
      //The values for spacing are in multiples of 8 (eg. 16,24,32... but it is not strict)
      //to add spacing an all sides use EdgeInsets.all().
      //To add spacing on certain sides, use EdgeInsets.only(direction: value)
      //EdgeInsets hover for details
      The state is lifted up:
      App.questionIdx =2
        -> BodyQuestions.questionIdx =1


 */
