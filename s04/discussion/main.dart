//The main()  function is the entry point of Dart application.
void main(){
  print(getCompanyName());
  print(getYearEstablished());
  print(getCoordinates());
  print(combineAddress('102 M. Roxas St.', 'Sta, Cruz', 'Lubao', 'Pampanga'));
  print(combineName('Ken', 'Mallari'));
  //optional named parameter:
  print(combineName('Ken', 'Mallari',isLastNameFirst: true));
  print(combineName('Ken', 'Mallari',isLastNameFirst: false));


  //Anonymous functions:
List<String> persons =['John Doe', 'Jane Doe'];
List<String> students = ['Nicholas Rush', 'James Holden'];

// persons.forEach((String person){
//   print(person);
// });
  

// students.forEach((String student){
//   print(student);
// });

//Functions as objects AND used as an arguments:

  //printName(value) is function exuctuion/call/invocation.
  //printName is reference to the given function
  // persons.forEach(printName);
  // students.forEach(printName);
  
}


void printName(String name){
  print (name);
}




//Optional named parameters: 
//These are the parameters added after the required ones.
//These parameters are added inside a curly bracket.
//If no value is given, a default value can be assigned
String combineName(String firstName, String lastName,{bool isLastNameFirst = false}){
  if(isLastNameFirst){
    return '$lastName, $firstName';
  }else{
    return '$firstName $lastName';
  }

}

String combineAddress(String specifics, String barangay, String city, String province){
  //return specifics +  ',' + barangay + ',' + city + ', ' + province;
  return '$specifics, $barangay, $city, $province';
}



String getCompanyName(){

  return 'FFUF';

  }

  int getYearEstablished(){
     return 2021;
  }
  bool hasOnlineClasses(){
    return true;
  }

  Map<String,double> getCoordinates(){
    return{
      'latutude': 14.62,
      'longitude': 121.04
    };
  }

  //The initial isUnderAge function can be changed into lambda (arrow function).
  // bool isUnderAge(int age){
  //   return (age < 18)? true : false;
  // }

  //Lambda function is a shortcut function for returning values from simple operations
  //Syntax: return-type function-name(parameters) => expression;
  bool isUnderAge(int age) => (age< 18) ? true : false;

  //kay dart when it comes to functions, when we create a parameter, the type of the parameter is not required.
  //writing a dtata type of function parameter is not required
  //the reason is related to code readability and conciseness.

  //The following syntax is followed when creating a function
/*
return-type function-name(param-data-type parameterA, param-data-type parameterB){
  //code logic inside a function
  return 'return-value';
}
arguments are the values being passed to the function.
parameters are the values being received by the function.
x =5;
x = parameter
5= arguments
*/
