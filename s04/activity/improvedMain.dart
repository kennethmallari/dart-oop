// A variable must be a noun
//A function should be a verb.
main(){
  List <num> prices = [45, 34.2, 176.9, 32.2];

  //This is to test the getTotal():
  num total = getTotalPrice(prices);
  print(total);

  //This is to test the getDiscount():
  print(getDiscount(total,20).toStringAsFixed(2));
  print(getDiscount(total,40).toStringAsFixed(2));
  print(getDiscount(total,60).toStringAsFixed(2));
  print(getDiscount(total,80).toStringAsFixed(2));
}

num getTotalPrice(List<num>prices){
    num total = 0;  
    prices.forEach((num price) => total += price);
    return total;  
}

num getDiscount(num total, num discountRate) =>(total -(total*discountRate) /100);

  

