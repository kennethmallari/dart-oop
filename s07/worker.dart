// class _Worker{
//     String _getType(){
//         return 'Worker';
//     }
// }

// // '_' will make it private(encapsulated). You can use it within the file, but not outside.


// //You can encapsulate a variable, method, property, class.class

// //To use:
// class Carpenter extends _Worker{
//     String getType(){
//         return super._getType();
//     }
// }

//Abstraction:
abstract class _Worker{
    String getFullName();
}

class Carpenter implements _Worker{
    String getFullName(){
        return 'Carpenter';
    }
}