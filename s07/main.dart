import './worker.dart';
void main(){
    //Person personA = new Person();
    Doctor doctor = new Doctor(firstName: 'Ken', lastName: 'Mallari');
    Carpenter worker = new Carpenter();

    print(worker.getFullName());
}
abstract class Person{
    
    
    //The class Person defines that a 'getFillName' must be implemented.
    //However, it does not tell the concrete HOW to implement it.
    //abstract method, am method that doesn't have a block of code.
    String getFullName();

}

//The concrete class is the Doctor class
class Doctor implements Person{
    String firstName;
    String lastName;

    Doctor({
        required this.firstName,
        required this.lastName
    });

    String getFullName(){
        return 'Dr. ';
    }

}
class Attorney implements Person{
    String getFullName(){
        return 'Atty';
    }
}