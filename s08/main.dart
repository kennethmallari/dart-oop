// Create an s08/main.dart file and put the comments here to the Dart file. ok

// Create an abstract class named Equipment and an 
// abstract method named describe inside the abstract class. ok

// Create the classes Bulldozer, TowerCrane, and Loader 
// that will all implement the abstract class Equipment. ok

// Create objects for each of the vehicle type with the following information.
// - Bulldozer: Caterpillar D10, U blade
// - Tower Crane: 370 EC-B 12 Fibre(model), 78m hook radius(radius), (capacity)12,000kg max capacity
// - Loader: Volvo L60H, wheel loader(equipment type), 16530lbs tipping load

// Inside the main() method, create a List for the 
// equipment (using the Equipment type) and give it an initial value of [].

// Add the created objects to the list created earlier.

// Loop through each vehicle in the list and execute the describe() method.

// Sample Output
// - The bulldozer Caterpillar D10 has a U blade.
// - The tower crane 370 EC-B 12 Fibre has a radius of 78 and a max capacity of 12000.
// - The loader (model)Volvo L60H is a wheel loader and has a tipping load of 16530 lbs.
//=====================================================================================

void main() {
    //Instantiations:
    Bulldozer bulldozer1 = new Bulldozer(
        type: 'Caterpillar D10', 
        bladeType: 'U blade'
    );

    TowerCrane towerCrane1 = new TowerCrane(
        type: '370 EC-B 12 Fibre', 
        radius: 78, 
        maxCapacity: 12000
    );

    Loader loader1 = new Loader(
        type: 'Volvo L60H',
        equipmentType: 'wheel loader', 
        tippingLoad: 1653
    );

    List <dynamic> listOfTypes  = [];

    listOfTypes.add(bulldozer1);
    listOfTypes.add(towerCrane1);
    listOfTypes.add(loader1);

    listOfTypes.forEach((type){
        print(type.describe());
    });
}

abstract class Equipment {
    String describe();
}


class Bulldozer implements Equipment{
    String type;
    String bladeType;

  Bulldozer({
    required this.type, 
    required this.bladeType
   })
   {
   
     print('A Bulldozer object has been created');
   }

    String describe() {
        return 'The bulldozer ${this.type} has a ${this.bladeType}.';
    }

}

class TowerCrane implements Equipment {

    String type;
    num radius;
    num maxCapacity;

    TowerCrane({
        required this.type, 
        required this.radius, 
        required this.maxCapacity
    })
    {
        print('A TowerCrane object has been created.');
    }

    String describe(){
        return 'The tower crane ${this.type} has a radius of ${this.radius.toString()} and a max capacity of ${this.maxCapacity.toString()}.';
    }

}

//Loader class:
class Loader implements Equipment{
    //instance variables:
    String type;
    String equipmentType;
    num tippingLoad;

    //Constructor
    Loader({required this.type, required this.equipmentType,  required this.tippingLoad}){
        print('A Loader object has been created');
    }

    //Method/s:
    //Volvo L60H, wheel loader(equipment type), 16530lbs tipping load
    String describe(){
        return 'The loader ${this.type} is a ${this.equipmentType} and has a tipping load of ${this.tippingLoad.toString()} lbs.';
    }

}




