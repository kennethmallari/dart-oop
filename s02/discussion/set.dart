void main(){
  //What is a set? is an unordered collection of unique items.
  //list is also an unordered values, but you can sort and have duplicate values.
  //Set <dataType> variableName = {values};

  Set <String> subContractors = {'Sonderhoff','Stahlscmidth'};
  print(subContractors);
  //When Set are useful? When you need to aggregate unique names
  //
  //Add:
  subContractors.add('Schweisstenchnik');
  subContractors.add('Kreatel');
  subContractors.add('Kunstoffe'); 
  print(subContractors);

  //Remove:
  subContractors.remove('Kunstoffe');
  print(subContractors);

  //to make a Set immutable, prefix 'const' at the beginning: const Set <String> subContractors = {'Sonderhoff','Stahlscmidth'};
}