import 'dart:js';

void main(){

  //Map<key dataType , valueDataType> variableName  = {}
  Map<String,String> address = {
    'specifics' : '221B Baker Street',
    'district': 'Marylebone',
    'city' : 'London',
    'country': 'UK'
  };
   
  //add:
  address['region'] = 'Europe';

  print(address);
  // List implements the concept of an JsArrayList allows duplicate values while sets do not
  // List can be accessed using index while set elements cannot be accessed using a numeric index(unordered)
  // Map implements the concept of an object
}