void main(){
  //List <data type> variableName = [values]
  List <int> discountRanges = [20,40,60,80];
  List <String>names = [
    'John',
    'Jane',
    'Tom'
  ];
  //ctrl Shift D >> copy lines of code
  print(discountRanges);
  print(names);

  //access
  print(discountRanges[2]);
  print(names[1]);

  //check for length:
  print(discountRanges.length);

  //Change value:
  names[0] = 'Jonathan';
  print(names[0]);

  //What if we dont want to change the values in the lists
  const List <String>names2 = [
    'John',
    'Jane',
    'Tom'
  ];

  //OR:

  List <String>names3 = const[
    'John',
    'Jane',
    'Tom'
  ];

  //scenarios where values in the list is immutable: marital status
   const List<String> maritalStatus = [
     'single',
     'married',
     'divorced',
     'widowed'
   ];

   //add elements at the end of a list:
   names.add('Doe');
   print(names);

   //add element at the beginning of a List:
   names.insert(0,'Johnny');
   print(names);

   //other properties:
   print(names.isEmpty); //false; this is a way to check if there are elements in a list.
   print(names.length == 0);
   print(names.isNotEmpty);
   print(names.first);
   print(names.last);
   print(names.reversed);

   //Methods:
   //names.sort((a,b)=>a.length.compareTo(b.length));
   names.sort();
   print(names);
   print(names.reversed);
  
}